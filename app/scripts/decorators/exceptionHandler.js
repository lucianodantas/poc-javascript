'use strict';

var exceptionHandler = function($provide) {
    $provide.decorator('$exceptionHandler',
        function($delegate, $injector) {

            return function(exception, cause) {
                console.info('EXCECAO Capturada. Name: ' + exception.name);
                var rScope = $injector.get('$rootScope');

                if(rScope && exception.name === 'NegocioException'){
                    console.info('EXCECAO NegocioException Capturada.');
                    rScope.$broadcast('NegocioException', exception.message);
                }else if (exception.name === 'ServerException'){
                    rScope.erroGerado = exception;
                    var location = $injector.get('$location');
                    location.url('/error');
                }else{
                    //Tratamento de erros javascript no frontend
                    $delegate(exception, cause);
                }

            };
        }
    );
};


module.exports = exceptionHandler;