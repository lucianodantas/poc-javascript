var app = require('../app');
require('../services/mensagemService');

app.directive('mensagem', function() {
    return {
        restrict: 'E',
        replace: true,
        controller: function($scope, mensagemService){
            $scope.limparMensagem = function() {
                mensagemService.limpar();
            };
        },
        templateUrl: '/views/directives/mensagem.html'
    }
});