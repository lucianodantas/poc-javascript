'use strict'
exports.routeChangeInterceptor = function(event, nextRoute, $scope, $cookieStore, $location){
  if (nextRoute && nextRoute.$$route) {
    //Configurações de template ao redirecionar o usuario
    if(nextRoute.$$route.originalPath == '/signin' || nextRoute.$$route.originalPath == '/signup' ||
      nextRoute.$$route.originalPath == '/forgot-password' || nextRoute.$$route.originalPath == '/lockme' ||
      nextRoute.$$route.originalPath == '/404' || nextRoute.$$route.originalPath == '/505'){
      $scope.templateFull = false;
    }else{
      $scope.templateFull = true;

      if (!$cookieStore.get('usuarioLogado')) { 
        event.preventDefault();
        $location.path('/signin');
      }
    }

  }
}
