
/**
 * Inteceptor acionado sempre que uma requisição/resposta http for realizada
 *
 * Este evento está registrado no arquivo app.js
 *
 */
var httpInterceptor = function($q, $log, $location, $injector, $rootScope) {
    var exceptions = require('../utils/exceptions');
    return {

        request: function(request) {
            console.info('reuqest realizado.');
            $rootScope.loader = true;
            request.headers['AUTH_TOKEN'] = '123456'  
            return request;
        },

        requestError : function(request){
            var rootScope = $injector.get('$rootScope');
            $rootScope.loader = false;

            //500 - INTERNAL SERVER ERROR
            console.info('request  erro: ' + JSON.stringify(response));
            if (response.status === 500 || response.status === 400 || response.status === 404){
                throw new exceptions.server('ServerException', response.data ? response.data.descricao : 'NOT FOUND');
            }else{
                throw new exceptions.server(response.data.nome, response.data.descricao);
            }


            return request;
        },

        response: function(response){
            var rootScope = $injector.get('$rootScope');
            $rootScope.loader = false;
            console.info('response realizado.');
            return response;
        },

        responseError: function(response){
            var rootScope = $injector.get('$rootScope');
            console.info('response erro: ' + JSON.stringify(response));
            $rootScope.loader = false;
            console.info('response erro. ' + response.status);
            var http = $injector.get('$http');

            //406 - NOT ACCEPTABLE. Obs.: O mais adequado seria  400 - BAD_REQUEST.
            //Porem o JAX_RS lanca alguns erros 400 que nao sao possiveis ser capturadas e transformadas em erro 500
            if(response.status === 406){
                throw new exceptions.negocio(response.data.descricao);
            }

            //500 - INTERNAL SERVER ERROR
            if (response.status === 500 || response.status === 400 || response.status === 404){
                throw new exceptions.server('ServerException', response.data ? response.data.descricao : 'NOT FOUND');
            }

            //401 UNAUTHORIZED
            if (response.status === 401) {
                /*
                 http.jsonp('http://localhost:8080/blank/rest/authenticate?callback=JSON_CALLBACK').then(
                 function(data, status, headers, config) {
                 // enviar para a página principal
                 window.location = "http://" + window.location.host + "/#/";
                 }
                 );
                 */
            }

            //403 Forbidden
            if(response.status === 403) {
                //enviar para tela de login ao invés de chamar o serviço de autenticação
                // window.location = "http://picc.cnpq.br/";

                /*
                 http.jsonp('http://localhost:8080/blank/rest/authenticate?callback=JSON_CALLBACK').then(
                 function(data, status, headers, config) {
                 // enviar para a página principal
                 window.location = "http://" + window.location.host + "/#/";
                 }
                 );
                 */
            }

            return response;
        }

    };
}

module.exports = httpInterceptor;