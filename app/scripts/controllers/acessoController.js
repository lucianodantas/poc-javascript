'use strict';
var app = require('../app');
require('../services/acessoService.js');
var exceptions = require('../utils/exceptions');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:LogInController
 * @description
 * # LogInCtrl
 * Controller of the maisSaudeApp
 */
app.controller('AcessoController',
    function ($scope, $rootScope, $location, $cookieStore, acessoService) {

      $scope.usuarioVO = {};

      /*
      * Efetua o login
      */
      $scope.login = function() {
        acessoService.autenticar($scope.usuarioVO).then( 
          function(usuario){
            console.info(JSON.stringify(usuario));
            $cookieStore.put('usuarioLogado', JSON.stringify(usuario));
            $scope.variaveis.usuarioLogado = usuario;  
            $location.url('/home');
          }
        ); 
      };

     /*
      * Efetua o logout
      */
      $scope.logout = function() {
        $cookieStore.put('usuarioLogado', null);
        $scope.variaveis.usuarioLogado = {};
        $location.url('/signin');
      }

    });
