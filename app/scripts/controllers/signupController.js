'use strict';
var app = require('../app');
require('../services/usuarioService.js');
require('../services/mensagemService');
require('../utils/exceptions');
/**
 * @ngdoc function
 * @name maisSaudeApp.controller:SignUpController
 * @description
 * # LogInCtrl
 * Controller of the maisSaudeApp
 */
app.controller('SignUpController',
  function ($scope, $location, usuarioService, mensagemService) {

    $scope.usuarioVO = {};



    $scope.salvar = function() {
 
      usuarioService.salvarUsuario($scope.usuarioVO).then(
      	function(usuario){
      		mensagemService.adicionar('Usuário cadastrado com sucesso', 'info');
      		$scope.usuarioVO = {};
      		$location.url('/signin');		
      	}
      );
      
    };

  });
