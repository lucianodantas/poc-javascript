'use strict';
var app = require('../app');
var interceptors = require('../interceptors/routeChangeInterceptor')
require('../services/mensagemService');
/**
 * @ngdoc function
 * @name maisSaudeApp.controller:MainController
 * @description
 * # MainController
 * Main Controller of the maisSaudeApp
 */
app.controller('MainController', function ($scope, $rootScope, $cookieStore, $location, mensagemService) {
  //Objeto responsável por manter escopo de comunicação entre vários controllers
  $scope.variaveis = {
    usuarioLogado : $cookieStore.get('usuarioLogado') ? JSON.parse($cookieStore.get('usuarioLogado')) : {}
  };

  $scope.validarFormulario = function (form){
    var error = form.$error;

    var mensagem = '';
    angular.forEach(error.required, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' Obrigatório. <br />';
      }
    });
    angular.forEach(error.minlength, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' inválido. Quantidade mínima de caracteres não informados. <br />';
      }
    });
    angular.forEach(error.maxlength, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' inválido. Quantidade máxima de caracteres não informados. <br />';
      }
    });
    angular.forEach(error.email, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' deve ser um e-mail válido. <br />';
      }
    });
    angular.forEach(error.parse, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' Inválido. <br />';
      }
    });


    if(mensagem.length > 0){
      mensagemService.adicionar(mensagem, 'error').exibir();
      return false;
    }

    return true;
  };

  /**
   * Redireciona o usuario para alguma rota
   *
   * @param route
   */
  $scope.redirect = function(route){
    $location.url(route);
  };

  //Filtro de mudança de páginas.
  $scope.templateFull = true;
  $scope.$on('$routeChangeStart', function(event, nextRoute){
    interceptors.routeChangeInterceptor(event, nextRoute, $scope, $cookieStore, $location);
  });


  $scope.atualizarCookieUsuario = function atualizarCookieUsuario(usuario){
      $cookieStore.put('usuarioLogado', usuario);
      $scope.variaveis.usuarioLogado = $cookieStore.get('usuario');
  }

});
