'use strict';
var app = require('../app');
var valueObjects = require('../utils/valueObjects');
require('../services/usuarioService');
require('../services/perfilService');
require('../services/mensagemService');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:UsuarioController
 * @description
 * # LogInCtrl
 * Controller of the ReviewMyText
 */
app.controller('UsuarioController',
  function ($scope, $rootScope, $location, $routeParams, ngTableParams,  usuarioService, perfilService, mensagemService) { 
    $scope.consultaVO = new valueObjects.ConsultaVO();
    $scope.consultaPerfilVO = new valueObjects.ConsultaVO();
    $scope.usuarioVO = {};

    if($routeParams.id != null){
      $scope.usuarioVO = usuarioService.getById($routeParams.id).then(
        function(response){
          console.info(JSON.stringify(response));
          $scope.usuarioVO = response.data;          
        }
      );
    };

    /**
     * Configuracao do ng-table
     * @type {ngTableParams}
     */
    $scope.tabelaConsultarUsuarios = new ngTableParams(
      {
        page: $scope.consultaVO.pagina,
        count: $scope.consultaVO.totalPorPagina
      },
      {
        total: $scope.consultaVO.total,
        counts : $scope.consultaVO.configuracaoTotalPagina,

        getData: function($defer, params){
          $scope.consultaVO.pagina = params.page();
          $scope.consultaVO.totalPorPagina = params.count();

          usuarioService.consultarUsuarios($scope.consultaVO).then(
            function (retorno) {
                $scope.consultaVO = retorno;
                params.total($scope.consultaVO.total);
                $defer.resolve($scope.consultaVO.dados);
            }
          );
        }
      });

    /**
     * Efetua a Consulta de usuario
     */
    $scope.consultar = function() {
      $scope.tabelaConsultarUsuarios.page(1);
      $scope.tabelaConsultarUsuarios.reload();
    };

    /**
     * Insere um novo usuario
     */
    $scope.salvar = function() { 
       usuarioService.salvarUsuario($scope.usuarioVO).then(
        function(usuario){
          mensagemService.adicionar('Usuário cadastrado com sucesso', 'info');
          $scope.usuarioVO = {};
          $location.url('/usuarios');    
        } 
      );
      
    }; 

        /**
     * Alterar usuario
     */
    $scope.alterar = function() {
      usuarioService.alterarUsuario($scope.usuarioVO).then(
        function(usuario){
          $scope.usuarioVO = {};
          mensagemService.adicionar('Usuário alterado com sucesso', 'info');
          $location.url('/usuarios');    
        } 
      );     
    };
 
    /**
     * Excluir Usuário
     */
    $scope.excluir = function(idUsuario) { 
       usuarioService.excluirUsuario(idUsuario).then(
        function(usuario){
          mensagemService.adicionar('Usuário excluído com sucesso', 'info').exibir();
          $scope.consultar();
        } 
      );
    };

    /*
    * Vincular Perfil ao usuario
    */
    /**
     * Configuracao do ng-table
     * @type {ngTableParams}
     */ 
    $scope.tabelaConsultarPerfis = new ngTableParams(
      {
        page: $scope.consultaPerfilVO.pagina,
        count: $scope.consultaPerfilVO.totalPorPagina
      },
      {
        total: $scope.consultaPerfilVO.total,
        counts : $scope.consultaPerfilVO.configuracaoTotalPagina,

        getData: function($defer, params){
          $scope.consultaPerfilVO.pagina = params.page();
          $scope.consultaPerfilVO.totalPorPagina = params.count();

           perfilService.consultarPerfis($scope.consultaPerfilVO).then(
              function (retorno) {
                  $scope.consultaPerfilVO = retorno;
                  params.total($scope.consultaPerfilVO.total);
                  $defer.resolve($scope.consultaPerfilVO.dados);
              }
            );
        }
      });

    $scope.selecionarPerfil = function(idPerfil){
      perfilService.getById(idPerfil).then(
        function(response){
          if(!$scope.usuarioVO.perfis){
            $scope.usuarioVO.perfis = [];
          }
          console.info(JSON.stringify(response.data));

          $scope.usuarioVO.perfis.push(response.data); 

          console.info(JSON.stringify($scope.usuarioVO.perfis));
          $('#myModal').modal('hide');
        }
      );

    };


  });
