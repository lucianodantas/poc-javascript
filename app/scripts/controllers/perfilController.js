'use strict';
var app = require('../app');
require('../services/perfilService');
var valueObjects = require('../utils/valueObjects');
require('../services/mensagemService');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:PerfilController
 * @description
 * # LogInCtrl
 * Controller of the ReviewMyText
 */
app.controller('PerfilController',
  function ($scope,$routeParams, $rootScope, $location, ngTableParams,  perfilService, mensagemService) {
    $scope.consultaVO = new valueObjects.ConsultaVO();
    $scope.perfilVO = {};

    if($routeParams.id != null){      
      $scope.perfilVO = perfilService.getById($routeParams.id).then(
        function(response){          
          $scope.perfilVO = response.data;          
        }
      );
    };

    /**
     * Configuracao do ng-table
     * @type {ngTableParams}
     */
    $scope.tabelaConsultarPerfis = new ngTableParams(
      {
        page: $scope.consultaVO.pagina,
        count: $scope.consultaVO.totalPorPagina
      },
      {
        total: $scope.consultaVO.total,
        counts : $scope.consultaVO.configuracaoTotalPagina,

        getData: function($defer, params){
          $scope.consultaVO.pagina = params.page();
          $scope.consultaVO.totalPorPagina = params.count();

           perfilService.consultarPerfis($scope.consultaVO).then(
              function (retorno) {
                  $scope.consultaVO = retorno;
                  params.total($scope.consultaVO.total);
                  $defer.resolve($scope.consultaVO.dados);
              }
            );
        }
      });

    /**
     * Efetua a Consulta de perfis
     */
    $scope.consultar = function() {
      $scope.tabelaConsultarPerfis.page(1);
      $scope.tabelaConsultarPerfis.reload();
    };

    /**
     * Insere um novo perfil
     */
    $scope.salvar = function() {
      perfilService.salvarPerfil($scope.perfilVO).then(
        function(perfil){
          mensagemService.adicionar('Perfil cadastrado com sucesso', 'info');
          $scope.perfilVO = {};
          $location.url('/perfis');
        } 
      );
    }
    /**
     * Insere um novo perfil
     */
    $scope.alterar = function() {
      perfilService.alterarPerfil($scope.perfilVO).then(
        function(perfil){
          $scope.perfilVO = {};
          mensagemService.adicionar('Perfil alterado com sucesso', 'info');
          $location.url('/perfis');
        } 
      );         
    }

    /**
     * Excluir perfil
     */
    $scope.excluir = function(idPerfil) {
      perfilService.excluirPerfil(idPerfil).then(
        function(usuario){
           mensagemService.adicionar('Perfil excluído com sucesso', 'info').exibir();
           $scope.perfilVO = {}; 
           $scope.consultar();
        } 
      ); 
    }



  });
