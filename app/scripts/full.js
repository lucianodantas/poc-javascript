(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"C:\\geap\\poc\\app\\scripts\\app.js":[function(require,module,exports){
'use strict';

/**
 * @ngdoc overview
 * @name maisSaudeApp
 * @description
 * # maisSaudeApp
 *
 * Main module of the application.
 */
var app = angular.module('reviewMyText', [
    //'ngAnimate',
    //'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ngTable',
    'pascalprecht.translate',
    'angularFileUpload',
    'ui.utils.masks'
]);

app.config(require('./routes'));

app.config(require('./utils/i18n'));
app.config(require('./decorators/exceptionHandler'));
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push(require('./interceptors/httpInterceptor'));
});


app.run(function($rootScope, mensagemService){
    $rootScope.$on('NegocioException',function(scope, exception){
        mensagemService.adicionar(exception, 'warning').exibir();
        console.info('Evento Negocio Capturado no service');
    });

    $rootScope.$on("$routeChangeSuccess", function() {
        mensagemService.exibir();
    });


});

module.exports = app;

require('./controllers/mainController');
require('./controllers/acessoController');
require('./controllers/signupController');
require('./controllers/usuarioController');
require('./controllers/perfilController');
require('./directives/mensagem');



},{"./controllers/acessoController":"C:\\geap\\poc\\app\\scripts\\controllers\\acessoController.js","./controllers/mainController":"C:\\geap\\poc\\app\\scripts\\controllers\\mainController.js","./controllers/perfilController":"C:\\geap\\poc\\app\\scripts\\controllers\\perfilController.js","./controllers/signupController":"C:\\geap\\poc\\app\\scripts\\controllers\\signupController.js","./controllers/usuarioController":"C:\\geap\\poc\\app\\scripts\\controllers\\usuarioController.js","./decorators/exceptionHandler":"C:\\geap\\poc\\app\\scripts\\decorators\\exceptionHandler.js","./directives/mensagem":"C:\\geap\\poc\\app\\scripts\\directives\\mensagem.js","./interceptors/httpInterceptor":"C:\\geap\\poc\\app\\scripts\\interceptors\\httpInterceptor.js","./routes":"C:\\geap\\poc\\app\\scripts\\routes.js","./utils/i18n":"C:\\geap\\poc\\app\\scripts\\utils\\i18n.js"}],"C:\\geap\\poc\\app\\scripts\\controllers\\acessoController.js":[function(require,module,exports){
'use strict';
var app = require('../app');
require('../services/acessoService.js');
var exceptions = require('../utils/exceptions');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:LogInController
 * @description
 * # LogInCtrl
 * Controller of the maisSaudeApp
 */
app.controller('AcessoController',
    function ($scope, $rootScope, $location, $cookieStore, acessoService) {

      $scope.usuarioVO = {};

      /*
      * Efetua o login
      */
      $scope.login = function() {
        acessoService.autenticar($scope.usuarioVO).then( 
          function(usuario){
            console.info(JSON.stringify(usuario));
            $cookieStore.put('usuarioLogado', JSON.stringify(usuario));
            $scope.variaveis.usuarioLogado = usuario;  
            $location.url('/home');
          }
        ); 
      };

     /*
      * Efetua o logout
      */
      $scope.logout = function() {
        $cookieStore.put('usuarioLogado', null);
        $scope.variaveis.usuarioLogado = {};
        $location.url('/signin');
      }

    });

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../services/acessoService.js":"C:\\geap\\poc\\app\\scripts\\services\\acessoService.js","../utils/exceptions":"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js"}],"C:\\geap\\poc\\app\\scripts\\controllers\\mainController.js":[function(require,module,exports){
'use strict';
var app = require('../app');
var interceptors = require('../interceptors/routeChangeInterceptor')
require('../services/mensagemService');
/**
 * @ngdoc function
 * @name maisSaudeApp.controller:MainController
 * @description
 * # MainController
 * Main Controller of the maisSaudeApp
 */
app.controller('MainController', function ($scope, $rootScope, $cookieStore, $location, mensagemService) {
  //Objeto responsável por manter escopo de comunicação entre vários controllers
  $scope.variaveis = {
    usuarioLogado : $cookieStore.get('usuarioLogado') ? JSON.parse($cookieStore.get('usuarioLogado')) : {}
  };

  $scope.validarFormulario = function (form){
    var error = form.$error;

    var mensagem = '';
    angular.forEach(error.required, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' Obrigatório. <br />';
      }
    });
    angular.forEach(error.minlength, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' inválido. Quantidade mínima de caracteres não informados. <br />';
      }
    });
    angular.forEach(error.maxlength, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' inválido. Quantidade máxima de caracteres não informados. <br />';
      }
    });
    angular.forEach(error.email, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' deve ser um e-mail válido. <br />';
      }
    });
    angular.forEach(error.parse, function(field){
      if(field.$invalid){
        mensagem = mensagem + 'Campo ' + field.$name + ' Inválido. <br />';
      }
    });


    if(mensagem.length > 0){
      mensagemService.adicionar(mensagem, 'error').exibir();
      return false;
    }

    return true;
  };

  /**
   * Redireciona o usuario para alguma rota
   *
   * @param route
   */
  $scope.redirect = function(route){
    $location.url(route);
  };

  //Filtro de mudança de páginas.
  $scope.templateFull = true;
  $scope.$on('$routeChangeStart', function(event, nextRoute){
    interceptors.routeChangeInterceptor(event, nextRoute, $scope, $cookieStore, $location);
  });


  $scope.atualizarCookieUsuario = function atualizarCookieUsuario(usuario){
      $cookieStore.put('usuarioLogado', usuario);
      $scope.variaveis.usuarioLogado = $cookieStore.get('usuario');
  }

});

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../interceptors/routeChangeInterceptor":"C:\\geap\\poc\\app\\scripts\\interceptors\\routeChangeInterceptor.js","../services/mensagemService":"C:\\geap\\poc\\app\\scripts\\services\\mensagemService.js"}],"C:\\geap\\poc\\app\\scripts\\controllers\\perfilController.js":[function(require,module,exports){
'use strict';
var app = require('../app');
require('../services/perfilService');
var valueObjects = require('../utils/valueObjects');
require('../services/mensagemService');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:PerfilController
 * @description
 * # LogInCtrl
 * Controller of the ReviewMyText
 */
app.controller('PerfilController',
  function ($scope,$routeParams, $rootScope, $location, ngTableParams,  perfilService, mensagemService) {
    $scope.consultaVO = new valueObjects.ConsultaVO();
    $scope.perfilVO = {};

    if($routeParams.id != null){      
      $scope.perfilVO = perfilService.getById($routeParams.id).then(
        function(response){          
          $scope.perfilVO = response.data;          
        }
      );
    };

    /**
     * Configuracao do ng-table
     * @type {ngTableParams}
     */
    $scope.tabelaConsultarPerfis = new ngTableParams(
      {
        page: $scope.consultaVO.pagina,
        count: $scope.consultaVO.totalPorPagina
      },
      {
        total: $scope.consultaVO.total,
        counts : $scope.consultaVO.configuracaoTotalPagina,

        getData: function($defer, params){
          $scope.consultaVO.pagina = params.page();
          $scope.consultaVO.totalPorPagina = params.count();

           perfilService.consultarPerfis($scope.consultaVO).then(
              function (retorno) {
                  $scope.consultaVO = retorno;
                  params.total($scope.consultaVO.total);
                  $defer.resolve($scope.consultaVO.dados);
              }
            );
        }
      });

    /**
     * Efetua a Consulta de perfis
     */
    $scope.consultar = function() {
      $scope.tabelaConsultarPerfis.page(1);
      $scope.tabelaConsultarPerfis.reload();
    };

    /**
     * Insere um novo perfil
     */
    $scope.salvar = function() {
      perfilService.salvarPerfil($scope.perfilVO).then(
        function(perfil){
          mensagemService.adicionar('Perfil cadastrado com sucesso', 'info');
          $scope.perfilVO = {};
          $location.url('/perfis');
        } 
      );
    }
    /**
     * Insere um novo perfil
     */
    $scope.alterar = function() {
      perfilService.alterarPerfil($scope.perfilVO).then(
        function(perfil){
          $scope.perfilVO = {};
          mensagemService.adicionar('Perfil alterado com sucesso', 'info');
          $location.url('/perfis');
        } 
      );         
    }

    /**
     * Excluir perfil
     */
    $scope.excluir = function(idPerfil) {
      perfilService.excluirPerfil(idPerfil).then(
        function(usuario){
           mensagemService.adicionar('Perfil excluído com sucesso', 'info').exibir();
           $scope.perfilVO = {}; 
           $scope.consultar();
        } 
      ); 
    }



  });

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../services/mensagemService":"C:\\geap\\poc\\app\\scripts\\services\\mensagemService.js","../services/perfilService":"C:\\geap\\poc\\app\\scripts\\services\\perfilService.js","../utils/valueObjects":"C:\\geap\\poc\\app\\scripts\\utils\\valueObjects.js"}],"C:\\geap\\poc\\app\\scripts\\controllers\\signupController.js":[function(require,module,exports){
'use strict';
var app = require('../app');
require('../services/usuarioService.js');
require('../services/mensagemService');
require('../utils/exceptions');
/**
 * @ngdoc function
 * @name maisSaudeApp.controller:SignUpController
 * @description
 * # LogInCtrl
 * Controller of the maisSaudeApp
 */
app.controller('SignUpController',
  function ($scope, $location, usuarioService, mensagemService) {

    $scope.usuarioVO = {};



    $scope.salvar = function() {
 
      usuarioService.salvarUsuario($scope.usuarioVO).then(
      	function(usuario){
      		mensagemService.adicionar('Usuário cadastrado com sucesso', 'info');
      		$scope.usuarioVO = {};
      		$location.url('/signin');		
      	}
      );
      
    };

  });

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../services/mensagemService":"C:\\geap\\poc\\app\\scripts\\services\\mensagemService.js","../services/usuarioService.js":"C:\\geap\\poc\\app\\scripts\\services\\usuarioService.js","../utils/exceptions":"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js"}],"C:\\geap\\poc\\app\\scripts\\controllers\\usuarioController.js":[function(require,module,exports){
'use strict';
var app = require('../app');
var valueObjects = require('../utils/valueObjects');
require('../services/usuarioService');
require('../services/perfilService');
require('../services/mensagemService');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:UsuarioController
 * @description
 * # LogInCtrl
 * Controller of the ReviewMyText
 */
app.controller('UsuarioController',
  function ($scope, $rootScope, $location, $routeParams, ngTableParams,  usuarioService, perfilService, mensagemService) { 
    $scope.consultaVO = new valueObjects.ConsultaVO();
    $scope.consultaPerfilVO = new valueObjects.ConsultaVO();
    $scope.usuarioVO = {};

    if($routeParams.id != null){
      $scope.usuarioVO = usuarioService.getById($routeParams.id).then(
        function(response){
          console.info(JSON.stringify(response));
          $scope.usuarioVO = response.data;          
        }
      );
    };

    /**
     * Configuracao do ng-table
     * @type {ngTableParams}
     */
    $scope.tabelaConsultarUsuarios = new ngTableParams(
      {
        page: $scope.consultaVO.pagina,
        count: $scope.consultaVO.totalPorPagina
      },
      {
        total: $scope.consultaVO.total,
        counts : $scope.consultaVO.configuracaoTotalPagina,

        getData: function($defer, params){
          $scope.consultaVO.pagina = params.page();
          $scope.consultaVO.totalPorPagina = params.count();

          usuarioService.consultarUsuarios($scope.consultaVO).then(
            function (retorno) {
                $scope.consultaVO = retorno;
                params.total($scope.consultaVO.total);
                $defer.resolve($scope.consultaVO.dados);
            }
          );
        }
      });

    /**
     * Efetua a Consulta de usuario
     */
    $scope.consultar = function() {
      $scope.tabelaConsultarUsuarios.page(1);
      $scope.tabelaConsultarUsuarios.reload();
    };

    /**
     * Insere um novo usuario
     */
    $scope.salvar = function() { 
       usuarioService.salvarUsuario($scope.usuarioVO).then(
        function(usuario){
          mensagemService.adicionar('Usuário cadastrado com sucesso', 'info');
          $scope.usuarioVO = {};
          $location.url('/usuarios');    
        } 
      );
      
    }; 

        /**
     * Alterar usuario
     */
    $scope.alterar = function() {
      usuarioService.alterarUsuario($scope.usuarioVO).then(
        function(usuario){
          $scope.usuarioVO = {};
          mensagemService.adicionar('Usuário alterado com sucesso', 'info');
          $location.url('/usuarios');    
        } 
      );     
    };
 
    /**
     * Excluir Usuário
     */
    $scope.excluir = function(idUsuario) { 
       usuarioService.excluirUsuario(idUsuario).then(
        function(usuario){
          mensagemService.adicionar('Usuário excluído com sucesso', 'info').exibir();
          $scope.consultar();
        } 
      );
    };

    /*
    * Vincular Perfil ao usuario
    */
    /**
     * Configuracao do ng-table
     * @type {ngTableParams}
     */ 
    $scope.tabelaConsultarPerfis = new ngTableParams(
      {
        page: $scope.consultaPerfilVO.pagina,
        count: $scope.consultaPerfilVO.totalPorPagina
      },
      {
        total: $scope.consultaPerfilVO.total,
        counts : $scope.consultaPerfilVO.configuracaoTotalPagina,

        getData: function($defer, params){
          $scope.consultaPerfilVO.pagina = params.page();
          $scope.consultaPerfilVO.totalPorPagina = params.count();

           perfilService.consultarPerfis($scope.consultaPerfilVO).then(
              function (retorno) {
                  $scope.consultaPerfilVO = retorno;
                  params.total($scope.consultaPerfilVO.total);
                  $defer.resolve($scope.consultaPerfilVO.dados);
              }
            );
        }
      });

    $scope.selecionarPerfil = function(idPerfil){
      perfilService.getById(idPerfil).then(
        function(response){
          if(!$scope.usuarioVO.perfis){
            $scope.usuarioVO.perfis = [];
          }
          console.info(JSON.stringify(response.data));

          $scope.usuarioVO.perfis.push(response.data); 

          console.info(JSON.stringify($scope.usuarioVO.perfis));
          $('#myModal').modal('hide');
        }
      );

    };


  });

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../services/mensagemService":"C:\\geap\\poc\\app\\scripts\\services\\mensagemService.js","../services/perfilService":"C:\\geap\\poc\\app\\scripts\\services\\perfilService.js","../services/usuarioService":"C:\\geap\\poc\\app\\scripts\\services\\usuarioService.js","../utils/valueObjects":"C:\\geap\\poc\\app\\scripts\\utils\\valueObjects.js"}],"C:\\geap\\poc\\app\\scripts\\decorators\\exceptionHandler.js":[function(require,module,exports){
'use strict';

var exceptionHandler = function($provide) {
    $provide.decorator('$exceptionHandler',
        function($delegate, $injector) {

            return function(exception, cause) {
                console.info('EXCECAO Capturada. Name: ' + exception.name);
                var rScope = $injector.get('$rootScope');

                if(rScope && exception.name === 'NegocioException'){
                    console.info('EXCECAO NegocioException Capturada.');
                    rScope.$broadcast('NegocioException', exception.message);
                }else if (exception.name === 'ServerException'){
                    rScope.erroGerado = exception;
                    var location = $injector.get('$location');
                    location.url('/error');
                }else{
                    //Tratamento de erros javascript no frontend
                    $delegate(exception, cause);
                }

            };
        }
    );
};


module.exports = exceptionHandler;
},{}],"C:\\geap\\poc\\app\\scripts\\directives\\mensagem.js":[function(require,module,exports){
var app = require('../app');
require('../services/mensagemService');

app.directive('mensagem', function() {
    return {
        restrict: 'E',
        replace: true,
        controller: function($scope, mensagemService){
            $scope.limparMensagem = function() {
                mensagemService.limpar();
            };
        },
        templateUrl: '/views/directives/mensagem.html'
    }
});
},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../services/mensagemService":"C:\\geap\\poc\\app\\scripts\\services\\mensagemService.js"}],"C:\\geap\\poc\\app\\scripts\\interceptors\\httpInterceptor.js":[function(require,module,exports){

/**
 * Inteceptor acionado sempre que uma requisição/resposta http for realizada
 *
 * Este evento está registrado no arquivo app.js
 *
 */
var httpInterceptor = function($q, $log, $location, $injector, $rootScope) {
    var exceptions = require('../utils/exceptions');
    return {

        request: function(request) {
            console.info('reuqest realizado.');
            $rootScope.loader = true;
            request.headers['AUTH_TOKEN'] = '123456'  
            return request;
        },

        requestError : function(request){
            var rootScope = $injector.get('$rootScope');
            $rootScope.loader = false;

            //500 - INTERNAL SERVER ERROR
            console.info('request  erro: ' + JSON.stringify(response));
            if (response.status === 500 || response.status === 400 || response.status === 404){
                throw new exceptions.server('ServerException', response.data ? response.data.descricao : 'NOT FOUND');
            }else{
                throw new exceptions.server(response.data.nome, response.data.descricao);
            }


            return request;
        },

        response: function(response){
            var rootScope = $injector.get('$rootScope');
            $rootScope.loader = false;
            console.info('response realizado.');
            return response;
        },

        responseError: function(response){
            var rootScope = $injector.get('$rootScope');
            console.info('response erro: ' + JSON.stringify(response));
            $rootScope.loader = false;
            console.info('response erro. ' + response.status);
            var http = $injector.get('$http');

            //406 - NOT ACCEPTABLE. Obs.: O mais adequado seria  400 - BAD_REQUEST.
            //Porem o JAX_RS lanca alguns erros 400 que nao sao possiveis ser capturadas e transformadas em erro 500
            if(response.status === 406){
                throw new exceptions.negocio(response.data.descricao);
            }

            //500 - INTERNAL SERVER ERROR
            if (response.status === 500 || response.status === 400 || response.status === 404){
                throw new exceptions.server('ServerException', response.data ? response.data.descricao : 'NOT FOUND');
            }

            //401 UNAUTHORIZED
            if (response.status === 401) {
                /*
                 http.jsonp('http://localhost:8080/blank/rest/authenticate?callback=JSON_CALLBACK').then(
                 function(data, status, headers, config) {
                 // enviar para a página principal
                 window.location = "http://" + window.location.host + "/#/";
                 }
                 );
                 */
            }

            //403 Forbidden
            if(response.status === 403) {
                //enviar para tela de login ao invés de chamar o serviço de autenticação
                // window.location = "http://picc.cnpq.br/";

                /*
                 http.jsonp('http://localhost:8080/blank/rest/authenticate?callback=JSON_CALLBACK').then(
                 function(data, status, headers, config) {
                 // enviar para a página principal
                 window.location = "http://" + window.location.host + "/#/";
                 }
                 );
                 */
            }

            return response;
        }

    };
}

module.exports = httpInterceptor;
},{"../utils/exceptions":"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js"}],"C:\\geap\\poc\\app\\scripts\\interceptors\\routeChangeInterceptor.js":[function(require,module,exports){
'use strict'
exports.routeChangeInterceptor = function(event, nextRoute, $scope, $cookieStore, $location){
  if (nextRoute && nextRoute.$$route) {
    //Configurações de template ao redirecionar o usuario
    if(nextRoute.$$route.originalPath == '/signin' || nextRoute.$$route.originalPath == '/signup' ||
      nextRoute.$$route.originalPath == '/forgot-password' || nextRoute.$$route.originalPath == '/lockme' ||
      nextRoute.$$route.originalPath == '/404' || nextRoute.$$route.originalPath == '/505'){
      $scope.templateFull = false;
    }else{
      $scope.templateFull = true;

      if (!$cookieStore.get('usuarioLogado')) { 
        event.preventDefault();
        $location.path('/signin');
      }
    }

  }
}

},{}],"C:\\geap\\poc\\app\\scripts\\routes.js":[function(require,module,exports){
'use strict';

module.exports = function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })
        .when('/usuarios', {
            templateUrl: 'views/usuario/consultarUsuarios.html',
            controller: 'UsuarioController'
        })

        .when('/usuarios/novo', {
            templateUrl: 'views/usuario/adicionarUsuario.html',
            controller: 'UsuarioController'
        }) 
        .when('/usuarios/alterar/:id', {
            templateUrl: 'views/usuario/alterarUsuario.html',
            controller: 'UsuarioController'
        })
        .when('/perfis', { 
            templateUrl: 'views/perfil/consultarPerfil.html',
            controller: 'PerfilController'
        })
        .when('/perfis/alterar/:id', {
            templateUrl: 'views/perfil/alterarPerfil.html',
            controller: 'PerfilController'
        })

        .when('/perfis/adicionar', {
            templateUrl: 'views/perfil/adicionarPerfil.html',
            controller: 'PerfilController'
        })
        
        .when('/signin', {
            templateUrl: 'views/acesso/login.html',
            controller: 'AcessoController'
        })
        .when('/signup', {
            templateUrl: 'views/acesso/signup.html',
            controller: 'SignUpController'
        })        
        .when('/404', {
            templateUrl: 'views/404.html',
            controller: 'MainController'
        })
        .when('/505', {
            templateUrl: 'views/505.html',
            controller: 'MainController'
        })
        .otherwise({
            redirectTo: '/'
        });
};

},{}],"C:\\geap\\poc\\app\\scripts\\services\\acessoService.js":[function(require,module,exports){
'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('acessoService',
    function ($http, $q) {

     /**
       * Autentica o Usuário utilizando login e senha.
       * @param usuarioVO - Objeto preenchido com email e senha
       */
      this.autenticar = function (usuarioVO) {  
        var deferred = $q.defer();

        $http.post('/api/sessao/autenticar', usuarioVO).success( 
          function(response){ 
            console.info('Response: ' + JSON.stringify(response));
            if(!response){   
              throw new exceptions.negocio('Usuário ou Senha Inválido(s)');
            } 
             
            deferred.resolve(response);  
          });

         return deferred.promise; 
      };

         
   
    });

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../utils/exceptions":"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js"}],"C:\\geap\\poc\\app\\scripts\\services\\mensagemService.js":[function(require,module,exports){
'use strict'; 
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('mensagemService', 
  function ($rootScope, $http, $q) { 
    var filaMensagens = [];
  	
  	$rootScope.mensagemAtual = {};
	
	/**
	*
	*
	*/
	this.adicionar = function(mensagem, tipo) {
        if(!tipo){
            tipo = 'info';
        }

		filaMensagens = [{"texto" : mensagem, "tipo" : tipo}];
		
		return this;
	};

	/**
	*
	*
	*/
	this.exibir = function() { 
		$rootScope.mensagemAtual = filaMensagens.shift() || {};
		return this;
	};

	/**
	*
	*
	*/ 
	this.limpar = function() {
		$rootScope.mensagemAtual = {};

		return this;
	};	

	

});

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../utils/exceptions":"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js"}],"C:\\geap\\poc\\app\\scripts\\services\\perfilService.js":[function(require,module,exports){
'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('perfilService',
    function ($http, $q) {

        /**
        * Recupera um pperfil pelo ID
        */
        this.getById = function(id){ 
          return $http.get('/api/perfil/getbyid/' + id);
        }; 


        
        /**
         * Salva um novo perfil
         * @param perfilVO - Objeto preenchido com nome e descricao
         */
        this.salvarPerfil = function(perfil){
          var deferred = $q.defer();
          
          $http.post('/api/perfil/save', perfil).success(  
            function(response){ 
              console.info('Response: ' + JSON.stringify(response));
              
              if(!response){   
                throw new exceptions.negocio('Perfil já existente.');
              }
               
              deferred.resolve(response.data);  
            });

            return deferred.promise;
        }; 

     
        /**
         * Altera os dados do perfil
         */
        this.alterarPerfil= function (perfil){
          var deferred = $q.defer();
          
          $http.put('/api/perfil/update/' + perfil.Id, perfil).success(  
            function(response){ 
              console.info('Response: ' + JSON.stringify(response));
              deferred.resolve(response);  
            });

            return deferred.promise;
        };

        /**
         * Exclui os dados do perfil
         */
        this.excluirPerfil= function (idPerfil){
          var deferred = $q.defer();
            
          $http.delete('/api/perfil/delete/' + idPerfil).success(  
            function(response){ 
              console.info('Response: ' + JSON.stringify(response));
              deferred.resolve(response);  
            });

            return deferred.promise;
        };

        this.consultarPerfis = function (consultaVO) {
          var filtros = ''; 
          if(consultaVO.filtros.nome){
            filtros = filtros + 'nome eq \'' + consultaVO.filtros.nome + '\'' 
          }
          if(consultaVO.filtros.descricao){ 
            if(filtros.length > 0){
              filtros = filtros + ' and ' 
            }
            filtros = filtros + 'descricao eq \'' + consultaVO.filtros.descricao + '\''  
          } 

          if(filtros.length > 0){
            filtros = '$filter=' + filtros;
          }

          return $http.get('/api/perfil/get?q=' + filtros + '$orderby=nome'  +
              '&page=' + ((consultaVO.pagina-1) * consultaVO.totalPorPagina) + 
              '&pageSize=' + consultaVO.totalPorPagina
              ).then(
            function (response) {  
                console.info(JSON.stringify(response));
                consultaVO.dados = response.data.perfis;

                //Configura a paginacao 
                consultaVO.total = response.data.count;
                //consultaVO.pagina = consultaVO.pagina + 1;

                return consultaVO;
            });
        };

    });

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../utils/exceptions":"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js"}],"C:\\geap\\poc\\app\\scripts\\services\\usuarioService.js":[function(require,module,exports){
'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('usuarioService',
  function ($http, $q) {

    /**
     * Recupera um usuario pelo ID
     */
    this.getById = function(id){ 
      return $http.get('/api/usuario/getbyid/' + id);
    }; 



    /**
     * Salva  um novo usuario
     * @param usuarioVO - Objeto preenchido com email e senha
     */
    this.salvarUsuario = function(usuario){
      var deferred = $q.defer();
      
      if(usuario.senha != usuario.senhaConfirmacao){
        throw new exceptions.negocio('As senhas não conferem. Informe novamente!');
      }

      $http.post('/api/usuario/save', usuario).success(  
        function(response){ 
          console.info('Response: ' + JSON.stringify(response));
          
          if(!response){   
            throw new exceptions.negocio('E-mail já cadastrado.');
          }
           
          deferred.resolve(response.data);  
        });

        return deferred.promise;
    }; 

 
    /**
     * Altera os dados do usuario
     */
    this.alterarUsuario= function (usuario){
      var deferred = $q.defer();
      
      if(usuario.senha != usuario.senhaConfirmacao){
        throw new exceptions.negocio('As senhas não conferem. Informe novamente!');
      }

      $http.put('/api/usuario/update/' + usuario.Id, usuario).success(  
        function(response){ 
          console.info('Response: ' + JSON.stringify(response));
          deferred.resolve(response);  
        });

        return deferred.promise;
    };

    /**
     * Exclui os dados do usuario
     */
    this.excluirUsuario= function (idUsuario){
      var deferred = $q.defer();
     
      $http.delete('/api/usuario/delete/' + idUsuario).success(  
        function(response){ 
          console.info('Response: ' + JSON.stringify(response));
          deferred.resolve(response);  
        });

        return deferred.promise;
    };

    /*
    * Consulta usuarios pelos filtros
    */
    this.consultarUsuarios = function (consultaVO) {
      var filtros = ''; 
      if(consultaVO.filtros.nome){
        filtros = filtros + 'nome eq \'' + consultaVO.filtros.nome + '\'' 
      }
      if(consultaVO.filtros.email){ 
        if(filtros.length > 0){
          filtros = filtros + ' and ' 
        }
        filtros = filtros + 'email eq \'' + consultaVO.filtros.email + '\''  
      } 

      if(filtros.length > 0){
        filtros = '$filter=' + filtros;
      }

      return $http.get('/api/usuario/get?q=' + filtros + '$orderby=nome'  +
          '&page=' + ((consultaVO.pagina-1) * consultaVO.totalPorPagina) + 
          '&pageSize=' + consultaVO.totalPorPagina
          ).then(
        function (response) {  
            console.info(JSON.stringify(response));
            consultaVO.dados = response.data.usuarios;

            //Configura a paginacao 
            consultaVO.total = response.data.count;
            //consultaVO.pagina = consultaVO.pagina + 1;

            return consultaVO;
        });
    };

  });

},{"../app":"C:\\geap\\poc\\app\\scripts\\app.js","../utils/exceptions":"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js"}],"C:\\geap\\poc\\app\\scripts\\utils\\exceptions.js":[function(require,module,exports){
'use strict';

function NegocioException(message) {
    this.name = 'NegocioException';
    this.message= message;
}
NegocioException.prototype = new Error();
NegocioException.prototype.constructor = NegocioException;

function ServerException(rootCause, stackTrace) {
    this.name = 'ServerException';
    this.message = rootCause;
    this.stackTrace = stackTrace;
}
ServerException.prototype = new Error();
ServerException.prototype.constructor = ServerException;

exports.negocio = NegocioException;
exports.server = ServerException; 

},{}],"C:\\geap\\poc\\app\\scripts\\utils\\i18n.js":[function(require,module,exports){
var mensagens = {}

mensagens.pt = {
    LABEL_PORTUGUES: 'Portugu\u00EAs',
    LABEL_INGLES: 'Ingl\u00EAs',

    /** Componente de Telefone **/
    TELEFONE_TIPO: 'Tipo',
    TELEFONE_DDD: 'DDD',
    TELEFONE_NUMERO: 'N\u00famero',
    TELEFONE_ADICIONAR_CONTATO: 'Adicionar contato',
    TELEFONE_SELECIONAR : 'Selecionar',
    TELEFONE_TELEFONE : 'Telefone',
    TELEFONE_EDITAR_RESUMO: 'Editar resumo',
    TELEFONE_FECHAR: 'Fechar',

    /** Botões **/
    BOTAO_ADICIONAR : 'Adicionar',
    BOTAO_EXCLUIR : 'Excluir',
    BOTAO_EDITAR : 'Editar',
    BOTAO_SALVAR : 'Salvar',
    BOTAO_CONCLUIR: 'Concluir',


    TITULO_OFICIALGENERAL:'Oficial General',
    TITULO_INFORMACOES_PESSOAIS: 'Informações Pessoais',


    NOME:'Nome',
    NOME_GUERRA:'Nome de Guerra',
    POSTO:'Posto',
    SOBRENOME:'Sobrenome',
    DATA_NASCIMENTO:'Data de Nascimento',
    SENHA:'Senha',
    SENHA_CONFIRMACAO:'Senha de Confirmação',
    EMAIL:'E-mail',
    DESCRICAO:'Descrição',


    ACEITO_OS:'Aceito os ',
    TERMOS_POLITICAS:'termos e políticas',
    POSSUI_CONTA:'Já possui conta? ',
    ACESSE:'Acesse',
    INSCREVA_SE:'Inscreva-se para sua conta na GEAP',
    LISTA_USUARIOS:'Lista de Usuários',
    LISTA_PERFIS:'Lista de Perfis',


    /**
     * Botoes
     */
    ADICIONAR:'Adcionar',
    CONSULTAR: 'Consultar',
    CANCELAR:'Cancelar',
    SALVAR:'Salvar',
    ACOES:'Ações',
    ALTERAR: 'Alterar',




    /**
     * Mensagem de negócio
     */
    EMAIL_CADASTRADO:'E-mail já cadastrado.',
    SENHA_NAO_CONFERE:'Essas senhas não coincidem.Por favor informar novamente!'
};

mensagens.en = {
    LABEL_PORTUGUES: 'Portuguese',
    LABEL_INGLES: "English",

   /** Componente de Telefone **/
    TELEFONE_TIPO: 'Tipo',
    TELEFONE_DDD: 'DDD',
    TELEFONE_NUMERO: 'N\u00famero',
    TELEFONE_ADICIONAR_CONTATO: 'Adicionar contato',
    TELEFONE_SELECIONAR : 'Selecionar',
    TELEFONE_TELEFONE : 'Telefone',
    TELEFONE_EDITAR_RESUMO: 'Editar resumo',
    TELEFONE_FECHAR: 'Fechar',

    /** Botões **/
    BOTAO_ADICIONAR : 'Adicionar',
    BOTAO_EXCLUIR : 'Excluir',
    BOTAO_EDITAR : 'Editar',
    BOTAO_SALVAR : 'Salvar',
    BOTAO_CONCLUIR: 'Concluir',


    TITULO_OFICIALGENERAL:'Oficial General',
    TITULO_INFORMACOES_PESSOAIS: 'Informações Pessoais',


    NOME:'Nome',
    NOME_GUERRA:'Nome de Guerra',
    POSTO:'Posto',
    SOBRENOME:'Sobrenome',
    DATA_NASCIMENTO:'Data de Nascimento',
    SENHA:'Senha',
    SENHA_CONFIRMACAO:'Senha de Confirmação',
    EMAIL:'E-mail',
    DESCRICAO:'Descrição',


    ACEITO_OS:'Aceito os ',
    TERMOS_POLITICAS:'termos e políticas',
    POSSUI_CONTA:'Já possui conta ',
    ACESSE:'Acesse',
    INSCREVA_SE:'Inscreva-se para sua conta HeyMetro',
    LISTA_USUARIOS:'Lista de Usuários',
    LISTA_PERFIS:'Lista de Perfis',


    /**
     * Botoes
     */
    ADICIONAR:'Adcionar',
    CONSULTAR: 'Consultar',
    CANCELAR:'Cancelar',
    SALVAR:'Salvar',
    ACOES:'Ações',
    ALTERAR: 'Alterar',




    /**
     * Mensagem de negócio
     */
    EMAIL_CADASTRADO:'E-mail já cadastrado.',
    SENHA_NAO_CONFERE:'Essas senhas não coincidem.Por favor informar novamente!'
};


module.exports = function ($translateProvider) {
    $translateProvider.translations('pt', mensagens.pt);
    $translateProvider.translations('en', mensagens.en);
    $translateProvider.preferredLanguage('pt');
};

},{}],"C:\\geap\\poc\\app\\scripts\\utils\\valueObjects.js":[function(require,module,exports){
'use strict';

/**
 * ConsultaVO
 * @param filtros
 *    Objeto com os campos para aplicar o filtro na consulta
 * @param dados
 *    Lista de objetos com o resultado da consulta
 * @param lazyLoad - Opcional
 *    Default = False
 * @param pagina - Opcional
 *    Pagina atual (paginacao)
 * @param totalPorPagina - Opcional
 *    Quantidade de registros por pagina
 */
function ConsultaVO(filtros, dados, lazyLoad, pagina, totalPorPagina) {
  this.filtros = Default(filtros, {});

  this.dados = Default(dados, {});
  this.total = 0;

  this.lazyLoad = Default(lazyLoad, false);

  this.pagina = Default(pagina, 1);
  this.totalPorPagina = Default(totalPorPagina, 5);

  this.configuracaoTotalPagina = [5, 10, 25];
}

ConsultaVO.prototype = new Object();
ConsultaVO.prototype.constructor = ConsultaVO;

exports.ConsultaVO = ConsultaVO;


/*
 * Funcoes utilitarias
 */
function Default(variable, new_value)
{
  if(new_value === undefined) {
    return (variable === undefined) ? null : variable;
  }

  return (variable === undefined) ? new_value : variable;
}

},{}]},{},["C:\\geap\\poc\\app\\scripts\\app.js"]);
