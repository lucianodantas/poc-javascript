'use strict';

/**
 * @ngdoc overview
 * @name maisSaudeApp
 * @description
 * # maisSaudeApp
 *
 * Main module of the application.
 */
var app = angular.module('reviewMyText', [
    //'ngAnimate',
    //'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ngTable',
    'pascalprecht.translate',
    'angularFileUpload',
    'ui.utils.masks'
]);

app.config(require('./routes'));

app.config(require('./utils/i18n'));
app.config(require('./decorators/exceptionHandler'));
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push(require('./interceptors/httpInterceptor'));
});


app.run(function($rootScope, mensagemService){
    $rootScope.$on('NegocioException',function(scope, exception){
        mensagemService.adicionar(exception, 'warning').exibir();
        console.info('Evento Negocio Capturado no service');
    });

    $rootScope.$on("$routeChangeSuccess", function() {
        mensagemService.exibir();
    });


});

module.exports = app;

require('./controllers/mainController');
require('./controllers/acessoController');
require('./controllers/signupController');
require('./controllers/usuarioController');
require('./controllers/perfilController');
require('./directives/mensagem');


