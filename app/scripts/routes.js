'use strict';

module.exports = function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })
        .when('/usuarios', {
            templateUrl: 'views/usuario/consultarUsuarios.html',
            controller: 'UsuarioController'
        })

        .when('/usuarios/novo', {
            templateUrl: 'views/usuario/adicionarUsuario.html',
            controller: 'UsuarioController'
        }) 
        .when('/usuarios/alterar/:id', {
            templateUrl: 'views/usuario/alterarUsuario.html',
            controller: 'UsuarioController'
        })
        .when('/perfis', { 
            templateUrl: 'views/perfil/consultarPerfil.html',
            controller: 'PerfilController'
        })
        .when('/perfis/alterar/:id', {
            templateUrl: 'views/perfil/alterarPerfil.html',
            controller: 'PerfilController'
        })

        .when('/perfis/adicionar', {
            templateUrl: 'views/perfil/adicionarPerfil.html',
            controller: 'PerfilController'
        })
        
        .when('/signin', {
            templateUrl: 'views/acesso/login.html',
            controller: 'AcessoController'
        })
        .when('/signup', {
            templateUrl: 'views/acesso/signup.html',
            controller: 'SignUpController'
        })        
        .when('/404', {
            templateUrl: 'views/404.html',
            controller: 'MainController'
        })
        .when('/505', {
            templateUrl: 'views/505.html',
            controller: 'MainController'
        })
        .otherwise({
            redirectTo: '/'
        });
};
