'use strict'; 
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('mensagemService', 
  function ($rootScope, $http, $q) { 
    var filaMensagens = [];
  	
  	$rootScope.mensagemAtual = {};
	
	/**
	*
	*
	*/
	this.adicionar = function(mensagem, tipo) {
        if(!tipo){
            tipo = 'info';
        }

		filaMensagens = [{"texto" : mensagem, "tipo" : tipo}];
		
		return this;
	};

	/**
	*
	*
	*/
	this.exibir = function() { 
		$rootScope.mensagemAtual = filaMensagens.shift() || {};
		return this;
	};

	/**
	*
	*
	*/ 
	this.limpar = function() {
		$rootScope.mensagemAtual = {};

		return this;
	};	

	

});
