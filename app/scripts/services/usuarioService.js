'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('usuarioService',
  function ($http, $q) {

    /**
     * Recupera um usuario pelo ID
     */
    this.getById = function(id){ 
      return $http.get('/api/usuario/getbyid/' + id);
    }; 



    /**
     * Salva  um novo usuario
     * @param usuarioVO - Objeto preenchido com email e senha
     */
    this.salvarUsuario = function(usuario){
      var deferred = $q.defer();
      
      if(usuario.senha != usuario.senhaConfirmacao){
        throw new exceptions.negocio('As senhas não conferem. Informe novamente!');
      }

      $http.post('/api/usuario/save', usuario).success(  
        function(response){ 
          console.info('Response: ' + JSON.stringify(response));
          
          if(!response){   
            throw new exceptions.negocio('E-mail já cadastrado.');
          }
           
          deferred.resolve(response.data);  
        });

        return deferred.promise;
    }; 

 
    /**
     * Altera os dados do usuario
     */
    this.alterarUsuario= function (usuario){
      var deferred = $q.defer();
      
      if(usuario.senha != usuario.senhaConfirmacao){
        throw new exceptions.negocio('As senhas não conferem. Informe novamente!');
      }

      $http.put('/api/usuario/update/' + usuario.Id, usuario).success(  
        function(response){ 
          console.info('Response: ' + JSON.stringify(response));
          deferred.resolve(response);  
        });

        return deferred.promise;
    };

    /**
     * Exclui os dados do usuario
     */
    this.excluirUsuario= function (idUsuario){
      var deferred = $q.defer();
     
      $http.delete('/api/usuario/delete/' + idUsuario).success(  
        function(response){ 
          console.info('Response: ' + JSON.stringify(response));
          deferred.resolve(response);  
        });

        return deferred.promise;
    };

    /*
    * Consulta usuarios pelos filtros
    */
    this.consultarUsuarios = function (consultaVO) {
      var filtros = ''; 
      if(consultaVO.filtros.nome){
        filtros = filtros + 'nome eq \'' + consultaVO.filtros.nome + '\'' 
      }
      if(consultaVO.filtros.email){ 
        if(filtros.length > 0){
          filtros = filtros + ' and ' 
        }
        filtros = filtros + 'email eq \'' + consultaVO.filtros.email + '\''  
      } 

      if(filtros.length > 0){
        filtros = '$filter=' + filtros;
      }

      return $http.get('/api/usuario/get?q=' + filtros + '$orderby=nome'  +
          '&page=' + ((consultaVO.pagina-1) * consultaVO.totalPorPagina) + 
          '&pageSize=' + consultaVO.totalPorPagina
          ).then(
        function (response) {  
            console.info(JSON.stringify(response));
            consultaVO.dados = response.data.usuarios;

            //Configura a paginacao 
            consultaVO.total = response.data.count;
            //consultaVO.pagina = consultaVO.pagina + 1;

            return consultaVO;
        });
    };

  });
