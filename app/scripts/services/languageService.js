/**
 * Created by emmanuel.kiametis on 22/04/2015.
 */
'use strict';
var app = require('../app');

app.service('languageService', function ($window, $translate, $cookies) {
    this.switchBrowserLanguage = function () {
        if($cookies &&  $cookies.language){
            $translate.use($cookies.language);
        } else {
            var language = ($window.navigator.userLanguage || $window.navigator.language);
            if (language && language.indexOf('pt') > -1) {
                $translate.use('pt');
            } else {
                $translate.use('en');
            }
        }
        $cookies.language = $translate.use();
    }

    this.changeBrowserLanguage = function(language) {
        $translate.use(language);
        $cookies.language = $translate.use();
    }

    this.getIdiomaCorrente = function(){
        return $translate.use();
    }
});