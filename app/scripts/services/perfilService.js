'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('perfilService',
    function ($http, $q) {

        /**
        * Recupera um pperfil pelo ID
        */
        this.getById = function(id){ 
          return $http.get('/api/perfil/getbyid/' + id);
        }; 


        
        /**
         * Salva um novo perfil
         * @param perfilVO - Objeto preenchido com nome e descricao
         */
        this.salvarPerfil = function(perfil){
          var deferred = $q.defer();
          
          $http.post('/api/perfil/save', perfil).success(  
            function(response){ 
              console.info('Response: ' + JSON.stringify(response));
              
              if(!response){   
                throw new exceptions.negocio('Perfil já existente.');
              }
               
              deferred.resolve(response.data);  
            });

            return deferred.promise;
        }; 

     
        /**
         * Altera os dados do perfil
         */
        this.alterarPerfil= function (perfil){
          var deferred = $q.defer();
          
          $http.put('/api/perfil/update/' + perfil.Id, perfil).success(  
            function(response){ 
              console.info('Response: ' + JSON.stringify(response));
              deferred.resolve(response);  
            });

            return deferred.promise;
        };

        /**
         * Exclui os dados do perfil
         */
        this.excluirPerfil= function (idPerfil){
          var deferred = $q.defer();
            
          $http.delete('/api/perfil/delete/' + idPerfil).success(  
            function(response){ 
              console.info('Response: ' + JSON.stringify(response));
              deferred.resolve(response);  
            });

            return deferred.promise;
        };

        this.consultarPerfis = function (consultaVO) {
          var filtros = ''; 
          if(consultaVO.filtros.nome){
            filtros = filtros + 'nome eq \'' + consultaVO.filtros.nome + '\'' 
          }
          if(consultaVO.filtros.descricao){ 
            if(filtros.length > 0){
              filtros = filtros + ' and ' 
            }
            filtros = filtros + 'descricao eq \'' + consultaVO.filtros.descricao + '\''  
          } 

          if(filtros.length > 0){
            filtros = '$filter=' + filtros;
          }

          return $http.get('/api/perfil/get?q=' + filtros + '$orderby=nome'  +
              '&page=' + ((consultaVO.pagina-1) * consultaVO.totalPorPagina) + 
              '&pageSize=' + consultaVO.totalPorPagina
              ).then(
            function (response) {  
                console.info(JSON.stringify(response));
                consultaVO.dados = response.data.perfis;

                //Configura a paginacao 
                consultaVO.total = response.data.count;
                //consultaVO.pagina = consultaVO.pagina + 1;

                return consultaVO;
            });
        };

    });
