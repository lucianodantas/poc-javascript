'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('acessoService',
    function ($http, $q) {

     /**
       * Autentica o Usuário utilizando login e senha.
       * @param usuarioVO - Objeto preenchido com email e senha
       */
      this.autenticar = function (usuarioVO) {  
        var deferred = $q.defer();

        $http.post('/api/sessao/autenticar', usuarioVO).success( 
          function(response){ 
            console.info('Response: ' + JSON.stringify(response));
            if(!response){   
              throw new exceptions.negocio('Usuário ou Senha Inválido(s)');
            } 
             
            deferred.resolve(response);  
          });

         return deferred.promise; 
      };

         
   
    });
