'use strict';

function NegocioException(message) {
    this.name = 'NegocioException';
    this.message= message;
}
NegocioException.prototype = new Error();
NegocioException.prototype.constructor = NegocioException;

function ServerException(rootCause, stackTrace) {
    this.name = 'ServerException';
    this.message = rootCause;
    this.stackTrace = stackTrace;
}
ServerException.prototype = new Error();
ServerException.prototype.constructor = ServerException;

exports.negocio = NegocioException;
exports.server = ServerException; 
