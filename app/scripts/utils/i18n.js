var mensagens = {}

mensagens.pt = {
    LABEL_PORTUGUES: 'Portugu\u00EAs',
    LABEL_INGLES: 'Ingl\u00EAs',

    /** Componente de Telefone **/
    TELEFONE_TIPO: 'Tipo',
    TELEFONE_DDD: 'DDD',
    TELEFONE_NUMERO: 'N\u00famero',
    TELEFONE_ADICIONAR_CONTATO: 'Adicionar contato',
    TELEFONE_SELECIONAR : 'Selecionar',
    TELEFONE_TELEFONE : 'Telefone',
    TELEFONE_EDITAR_RESUMO: 'Editar resumo',
    TELEFONE_FECHAR: 'Fechar',

    /** Botões **/
    BOTAO_ADICIONAR : 'Adicionar',
    BOTAO_EXCLUIR : 'Excluir',
    BOTAO_EDITAR : 'Editar',
    BOTAO_SALVAR : 'Salvar',
    BOTAO_CONCLUIR: 'Concluir',


    TITULO_OFICIALGENERAL:'Oficial General',
    TITULO_INFORMACOES_PESSOAIS: 'Informações Pessoais',


    NOME:'Nome',
    NOME_GUERRA:'Nome de Guerra',
    POSTO:'Posto',
    SOBRENOME:'Sobrenome',
    DATA_NASCIMENTO:'Data de Nascimento',
    SENHA:'Senha',
    SENHA_CONFIRMACAO:'Senha de Confirmação',
    EMAIL:'E-mail',
    DESCRICAO:'Descrição',


    ACEITO_OS:'Aceito os ',
    TERMOS_POLITICAS:'termos e políticas',
    POSSUI_CONTA:'Já possui conta? ',
    ACESSE:'Acesse',
    INSCREVA_SE:'Inscreva-se para sua conta na GEAP',
    LISTA_USUARIOS:'Lista de Usuários',
    LISTA_PERFIS:'Lista de Perfis',


    /**
     * Botoes
     */
    ADICIONAR:'Adcionar',
    CONSULTAR: 'Consultar',
    CANCELAR:'Cancelar',
    SALVAR:'Salvar',
    ACOES:'Ações',
    ALTERAR: 'Alterar',




    /**
     * Mensagem de negócio
     */
    EMAIL_CADASTRADO:'E-mail já cadastrado.',
    SENHA_NAO_CONFERE:'Essas senhas não coincidem.Por favor informar novamente!'
};

mensagens.en = {
    LABEL_PORTUGUES: 'Portuguese',
    LABEL_INGLES: "English",

   /** Componente de Telefone **/
    TELEFONE_TIPO: 'Tipo',
    TELEFONE_DDD: 'DDD',
    TELEFONE_NUMERO: 'N\u00famero',
    TELEFONE_ADICIONAR_CONTATO: 'Adicionar contato',
    TELEFONE_SELECIONAR : 'Selecionar',
    TELEFONE_TELEFONE : 'Telefone',
    TELEFONE_EDITAR_RESUMO: 'Editar resumo',
    TELEFONE_FECHAR: 'Fechar',

    /** Botões **/
    BOTAO_ADICIONAR : 'Adicionar',
    BOTAO_EXCLUIR : 'Excluir',
    BOTAO_EDITAR : 'Editar',
    BOTAO_SALVAR : 'Salvar',
    BOTAO_CONCLUIR: 'Concluir',


    TITULO_OFICIALGENERAL:'Oficial General',
    TITULO_INFORMACOES_PESSOAIS: 'Informações Pessoais',


    NOME:'Nome',
    NOME_GUERRA:'Nome de Guerra',
    POSTO:'Posto',
    SOBRENOME:'Sobrenome',
    DATA_NASCIMENTO:'Data de Nascimento',
    SENHA:'Senha',
    SENHA_CONFIRMACAO:'Senha de Confirmação',
    EMAIL:'E-mail',
    DESCRICAO:'Descrição',


    ACEITO_OS:'Aceito os ',
    TERMOS_POLITICAS:'termos e políticas',
    POSSUI_CONTA:'Já possui conta ',
    ACESSE:'Acesse',
    INSCREVA_SE:'Inscreva-se para sua conta HeyMetro',
    LISTA_USUARIOS:'Lista de Usuários',
    LISTA_PERFIS:'Lista de Perfis',


    /**
     * Botoes
     */
    ADICIONAR:'Adcionar',
    CONSULTAR: 'Consultar',
    CANCELAR:'Cancelar',
    SALVAR:'Salvar',
    ACOES:'Ações',
    ALTERAR: 'Alterar',




    /**
     * Mensagem de negócio
     */
    EMAIL_CADASTRADO:'E-mail já cadastrado.',
    SENHA_NAO_CONFERE:'Essas senhas não coincidem.Por favor informar novamente!'
};


module.exports = function ($translateProvider) {
    $translateProvider.translations('pt', mensagens.pt);
    $translateProvider.translations('en', mensagens.en);
    $translateProvider.preferredLanguage('pt');
};
