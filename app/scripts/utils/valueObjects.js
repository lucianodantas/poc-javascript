'use strict';

/**
 * ConsultaVO
 * @param filtros
 *    Objeto com os campos para aplicar o filtro na consulta
 * @param dados
 *    Lista de objetos com o resultado da consulta
 * @param lazyLoad - Opcional
 *    Default = False
 * @param pagina - Opcional
 *    Pagina atual (paginacao)
 * @param totalPorPagina - Opcional
 *    Quantidade de registros por pagina
 */
function ConsultaVO(filtros, dados, lazyLoad, pagina, totalPorPagina) {
  this.filtros = Default(filtros, {});

  this.dados = Default(dados, {});
  this.total = 0;

  this.lazyLoad = Default(lazyLoad, false);

  this.pagina = Default(pagina, 1);
  this.totalPorPagina = Default(totalPorPagina, 5);

  this.configuracaoTotalPagina = [5, 10, 25];
}

ConsultaVO.prototype = new Object();
ConsultaVO.prototype.constructor = ConsultaVO;

exports.ConsultaVO = ConsultaVO;


/*
 * Funcoes utilitarias
 */
function Default(variable, new_value)
{
  if(new_value === undefined) {
    return (variable === undefined) ? null : variable;
  }

  return (variable === undefined) ? new_value : variable;
}
