(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

/**
 * @ngdoc overview
 * @name maisSaudeApp
 * @description
 * # maisSaudeApp
 *
 * Main module of the application.
 */
var app = angular.module('maisSaudeApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'ngTable'
]);

app.config(require('./routes'));
app.config(require('./decorators/exceptionHandler'));

module.exports = app;

require('./controllers/mainController');
require('./controllers/acessoController');
require('./controllers/signupController');
require('./controllers/medicoController');



},{"./controllers/acessoController":2,"./controllers/mainController":3,"./controllers/medicoController":4,"./controllers/signupController":5,"./decorators/exceptionHandler":6,"./routes":8}],2:[function(require,module,exports){
'use strict';
var app = require('../app');
require('../services/acessoService.js');
var exceptions = require('../utils/exceptions');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:LogInController
 * @description
 * # LogInCtrl
 * Controller of the maisSaudeApp
 */
app.controller('AcessoController',
  function ($scope, $rootScope, $location, $cookieStore, acessoService) {

    $scope.usuarioVO = {};

    $scope.login = function() {
      var usuarioVO = acessoService.consultarUsuarioPorEmailESenha(
        $scope.usuarioVO.email, $scope.usuarioVO.senha);

      if(usuarioVO === null){
        throw new exceptions.negocio('Usuário ou Senha Inválido(s)');
      }

      $cookieStore.put('usuarioLogado', usuarioVO);
      $scope.variaveis.usuarioLogado = $cookieStore.get('usuarioLogado');

      $location.url('/home');
    }

    $scope.logout = function() {
      $cookieStore.put('usuarioLogado', null);
      $scope.variaveis.usuarioLogado = {};
      $location.url('/signin');
    }



  });

},{"../app":1,"../services/acessoService.js":9,"../utils/exceptions":12}],3:[function(require,module,exports){
'use strict';
var app = require('../app');
var interceptors = require('../interceptors/routeChangeInterceptor')
/**
 * @ngdoc function
 * @name maisSaudeApp.controller:MainController
 * @description
 * # MainController
 * Main Controller of the maisSaudeApp
 */
app.controller('MainController', function ($scope, $rootScope, $cookieStore, $location) {
  var filaMensagens = [];


  //Objeto responsável por manter escopo de comunicação entre vários controllers
  $scope.variaveis = {
    usuarioLogado : $cookieStore.get('usuarioLogado'),
    mensagemAtual : {}
  };


  $scope.addMensagem = function(mensagem, tipo) {
    filaMensagens = [{texto: mensagem, tipo: tipo}];
  };

  $scope.atualizarMensagem = function() {
    $scope.variaveis.mensagemAtual = filaMensagens.shift() || "";
  };

  $scope.limparMensagem = function() {
    $scope.variaveis.mensagemAtual = {};
  };




  /**
   * Redireciona o usuario para alguma rota
   *
   * @param route
   */
  $scope.redirect = function(route){
    $location.url(route);
  };


  $scope.$on('NegocioException',function(scope, exception){
    $scope.addMensagem(exception, 'warning');
    $scope.atualizarMensagem();
  });



  //Filtro de mudança de páginas.
  $scope.templateFull = true;
  $scope.$on('$routeChangeStart', function(event, nextRoute){
    interceptors.routeChangeInterceptor(event, nextRoute, $scope, $cookieStore, $location);
  });
  $rootScope.$on("$routeChangeSuccess", function() {
    $scope.atualizarMensagem();
  });

});

},{"../app":1,"../interceptors/routeChangeInterceptor":7}],4:[function(require,module,exports){
'use strict';
var app = require('../app');
require('../services/medicoService.js');
require('../services/especialidadeService.js');
var valueObjects = require('../utils/valueObjects');

/**
 * @ngdoc function
 * @name maisSaudeApp.controller:LogInController
 * @description
 * # LogInCtrl
 * Controller of the maisSaudeApp
 */
app.controller('MedicoController',
  function ($scope, $rootScope, $location, ngTableParams, medicoService, especialidadeService) {
    $scope.consultaVO = new valueObjects.ConsultaVO();
    $scope.especialidades = especialidadeService.consultar(new valueObjects.ConsultaVO()).dados;
    $scope.medicoVO = {};

    /**
     * Configuracao do ng-table
     * @type {ngTableParams}
     */
    $scope.tabelaConsultarMedicos = new ngTableParams(
      {
        page: $scope.consultaVO.pagina,
        count: $scope.consultaVO.totalPorPagina
      },
      {
        total: $scope.consultaVO.total,
        counts : $scope.consultaVO.configuracaoTotalPagina,

        getData: function($defer, params){
          $scope.consultaVO.pagina = params.page();
          $scope.consultaVO.totalPorPagina = params.count();
          $scope.consultaVO = medicoService.consultarMedicos($scope.consultaVO);
          params.total($scope.consultaVO.total);
          $defer.resolve($scope.consultaVO.dados);
        }
      });

    /**
     * Efetua a Consulta de Medicos
     */
    $scope.consultar = function() {
      $scope.tabelaConsultarMedicos.page(1);
      $scope.tabelaConsultarMedicos.reload();
    };

    /**
     * Insere um novo medico
     */
    $scope.salvar = function() {
      $scope.medicoVO.especialidade = especialidadeService.consultar(
        new valueObjects.ConsultaVO(
          {id: $scope.medicoVO.especialidade.id}
      )).dados[0];

      medicoService.salvar($scope.medicoVO);

      $scope.medicoVO = {};
      $scope.addMensagem('Médico cadastrado com sucesso', 'info');

      $location.url('/medicos');
    }




  });

},{"../app":1,"../services/especialidadeService.js":10,"../services/medicoService.js":11,"../utils/valueObjects":13}],5:[function(require,module,exports){
'use strict';
var app = require('../app');
require('../services/acessoService.js');
/**
 * @ngdoc function
 * @name maisSaudeApp.controller:SignUpController
 * @description
 * # LogInCtrl
 * Controller of the maisSaudeApp
 */
app.controller('SignUpController',
  function ($scope, $location, acessoService) {

    $scope.usuarioVO = {};

    $scope.salvar = function() {
      acessoService.salvarUsuario($scope.usuarioVO);

      $scope.addMensagem('Usuário cadastrado com sucesso', 'info');
      $location.url('/signin')
    };

  });

},{"../app":1,"../services/acessoService.js":9}],6:[function(require,module,exports){
'use strict';

module.exports =  function($provide) {
    $provide.decorator('$exceptionHandler',
      function($delegate, $injector) {
        return function(exception, cause) {
          var rScope = $injector.get('$rootScope');
          if(rScope && exception.name == 'NegocioException'){
            rScope.$broadcast('NegocioException', exception.message);
          }else{
            $delegate(exception, cause);
          }
        };
      }
    );
}

},{}],7:[function(require,module,exports){
'use strict'
exports.routeChangeInterceptor = function(event, nextRoute, $scope, $cookieStore, $location){
  if (nextRoute && nextRoute.$$route) {
    //Configurações de template ao redirecionar o usuario
    if(nextRoute.$$route.originalPath == '/signin' || nextRoute.$$route.originalPath == '/signup' ||
      nextRoute.$$route.originalPath == '/forgot-password' || nextRoute.$$route.originalPath == '/lockme' ||
      nextRoute.$$route.originalPath == '/404' || nextRoute.$$route.originalPath == '/505'){
      $scope.templateFull = false;
    }else{
      $scope.templateFull = true;

      if ($cookieStore.get('usuarioLogado') == null) {
        event.preventDefault();
        $location.path('/signin');
      }
    }

  }
}

},{}],8:[function(require,module,exports){
'use strict';

module.exports = function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/home.html',
      controller: 'MainController'
    })
    .when('/medicos', {
      templateUrl: 'views/medico/consultarMedicos.html',
      controller: 'MedicoController'
    })
    .when('/medicos/adicionar', {
      templateUrl: 'views/medico/adicionarMedico.html',
      controller: 'MedicoController'
    })
    .when('/arrow', {
      templateUrl: 'views/arrow.html',
      controller: 'MainController'
    })
    .when('/badgeLabel', {
      templateUrl: 'views/badgeLabel.html',
      controller: 'MainController'
    })
    .when('/button', {
      templateUrl: 'views/button.html',
      controller: 'MainController'
    })
    .when('/color', {
      templateUrl: 'views/color.html',
      controller: 'MainController'
    })
    .when('/grid', {
      templateUrl: 'views/grid.html',
      controller: 'MainController'
    })
    .when('/icon', {
      templateUrl: 'views/icon.html',
      controller: 'MainController'
    })
    .when('/list', {
      templateUrl: 'views/list.html',
      controller: 'MainController'
    })
    .when('/nav', {
      templateUrl: 'views/nav.html',
      controller: 'MainController'
    })
    .when('/panel', {
      templateUrl: 'views/panel.html',
      controller: 'MainController'
    })
    .when('/progressbar', {
      templateUrl: 'views/progressbar.html',
      controller: 'MainController'
    })
    .when('/streamline', {
      templateUrl: 'views/streamline.html',
      controller: 'MainController'
    })
    .when('/timeline', {
      templateUrl: 'views/timeline.html',
      controller: 'MainController'
    })
    .when('/formLayout', {
      templateUrl: 'views/formLayout.html',
      controller: 'MainController'
    })
    .when('/formElement', {
      templateUrl: 'views/formElement.html',
      controller: 'MainController'
    })
    .when('/table', {
      templateUrl: 'views/table.html',
      controller: 'MainController'
    })
    .when('/email', {
      templateUrl: 'views/email.html',
      controller: 'MainController'
    })
    .when('/profile', {
      templateUrl: 'views/profile.html',
      controller: 'MainController'
    })
    .when('/settings', {
      templateUrl: 'views/settings.html',
      controller: 'MainController'
    })
    .when('/blank', {
      templateUrl: 'views/blank.html',
      controller: 'MainController'
    })
    .when('/signin', {
      templateUrl: 'views/acesso/login.html',
      controller: 'AcessoController'
    })
    .when('/signup', {
      templateUrl: 'views/acesso/signup.html',
      controller: 'SignUpController'
    })
    .when('/forgot-password', {
      templateUrl: 'views/acesso/forgot-password.html',
      controller: 'MainController'
    })
    .when('/lockme', {
      templateUrl: 'views/acesso/lockme.html',
      controller: 'MainController'
    })
    .when('/404', {
      templateUrl: 'views/404.html',
      controller: 'MainController'
    })
    .when('/505', {
      templateUrl: 'views/505.html',
      controller: 'MainController'
    })
    .otherwise({
      redirectTo: '/'
    });
};

},{}],9:[function(require,module,exports){
'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('acessoService',
  function () {

    this.usuarios = [
      {
        "nome" : "Luciano Assis Dantas",
        "email" : "luciano87@gmail.com",
        "senha": "123"
      },
      {
        "nome" : "Luciano Assis Dantas",
        "email" : "luciano87@outlook.com",
        "senha": "123"
      },
      {
        "nome" : "Luciano Assis Dantas",
        "email" : "luciano87@yahoo.com",
        "senha": "123"
      }
    ];

    /**
     * Autentica o Usuário utilizando login e senha.
     * @param usuarioVO - Objeto preenchido com email e senha
     */
    this.consultarUsuarioPorEmailESenha = function (email, senha) {
      var usuario = null;

      jQuery.each(this.usuarios, function() {
        if(email == this.email &&
          senha == this.senha){
          usuario = this;
          return false;
        }
      });

      return usuario;
    };

    /**
    * Autentica o Usuário utilizando login e senha.
    * @param usuarioVO - Objeto preenchido com email e senha
    */
    this.salvarUsuario = function(usuario){

      var usuarioDuplicado = _.find(this.usuarios, function(usuarioSalvo){
        return usuarioSalvo.email === usuario.email;
      });


      if(usuarioDuplicado != null){
        throw new exceptions.negocio('E-mail já cadastrado.');
      }


      this.usuarios.push(usuario);
    }

  });

},{"../app":1,"../utils/exceptions":12}],10:[function(require,module,exports){
'use strict';
var app = require('../app');

app.service('especialidadeService',
  function () {

    this.especialidades = [
      {id: 1, nome: "Cardiologia"},
      {id: 2, nome: "Otorrinolaringologista"},
      {id: 3, nome: "Pediatria"},
      {id: 4, nome: "Psiquiatria"},
      {id: 5, nome: "Urologia"}
    ];

    /**
     * Consulta todas as especialidades
     * @param consultaVO
     * @returns {*}
     */
    this.consultar = function (consultaVO) {
      consultaVO.dados = this.especialidades;

      //Aplica os filtros
      consultaVO.dados = _.where(consultaVO.dados, consultaVO.filtros);

      consultaVO.total = consultaVO.dados.length;

      //Configura a paginacao
      if(consultaVO.lazyLoad){
        consultaVO.dados =  consultaVO.dados.slice((consultaVO.pagina - 1) * consultaVO.totalPorPagina,
          consultaVO.pagina * consultaVO.totalPorPagina);
      }

      return consultaVO;
    };



  });

},{"../app":1}],11:[function(require,module,exports){
'use strict';
var app = require('../app');
var exceptions = require('../utils/exceptions');

app.service('medicoService',
  function () {

    this.medicos = [
      {id: 1, nome: "Tiancum", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 2, nome: "Jacob", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 3, nome: "Nephi", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 3, nome: "Pediatria"}},
      {id: 4, nome: "Xororó", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 5, nome: "Joao", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 6, nome: "Xico", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 7, nome: "Jose", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 8, nome: "Alfredo", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 9, nome: "Zacarias", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 3, nome: "Pediatria"}},
      {id: 10, nome: "Miguel", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 11, nome: "Raphael", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 12, nome: "Gabriel", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 13, nome: "Uriel", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 14, nome: "Zadkiel", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 15, nome: "Metatron", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 16, nome: "Virtue", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 17, nome: "Dorren", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 3, nome: "Pediatria"}},
      {id: 18, nome: "Lero", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 19, nome: "Lero-Lero", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 20, nome: "Lalala", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 21, nome: "LalalaLa", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 22, nome: "Mane", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 23, nome: "Garrincha", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 24, nome: "Brasilia", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 25, nome: "Campina", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 26, nome: "Grande", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 27, nome: "Joao", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 28, nome: "Pessoa", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 29, nome: "Patos", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 30, nome: "PB", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 31, nome: "Paraiba", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 32, nome: "Xororo", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}},
      {id: 33, nome: "Luciano", telefone: "123123213", email: "asdasd@asd", especialidade: {id: 1, nome: "Cardiologia"}}
    ];

    /**
     * Autentica o Usuário utilizando login e senha.
     * @param usuarioVO - Objeto preenchido com email e senha
     */
    this.consultarMedicos = function (consultaVO) {
      consultaVO.dados = this.medicos;

      //Aplica os filtros
      consultaVO.dados = _.filter(consultaVO.dados,
          function(medico) {

            if(consultaVO.filtros.nome != null && consultaVO.filtros.nome.length > 0 &&
                medico.nome != consultaVO.filtros.nome) {
              return false;
            }

            if(consultaVO.filtros.especialidade != null &&
              consultaVO.filtros.especialidade.id != null &&
              consultaVO.filtros.especialidade.id != medico.especialidade.id) {
              return false;
            }

            return true;
          });

      //Configura a paginacao
      consultaVO.total = consultaVO.dados.length;
      consultaVO.dados =  consultaVO.dados.slice((consultaVO.pagina - 1) * consultaVO.totalPorPagina,
        consultaVO.pagina * consultaVO.totalPorPagina);

      return consultaVO;
    };

    /**
    * Autentica o Usuário utilizando login e senha.
    * @param usuarioVO - Objeto preenchido com email e senha
    */
    this.salvar = function(novoMedico){
      var medicoExistente = _.find(this.medicos, function(medicoSalvo){
        return medicoSalvo.email === novoMedico.email;
      });

      if(medicoExistente != null){
        throw new exceptions.negocio('E-mail já cadastrado.');
      }

      var ultimoMedico = _.max(this.medicos, function(medico){ return medico.id; });
      novoMedico.id = ultimoMedico.id + 1;
      this.medicos.push(novoMedico);
    }

  });

},{"../app":1,"../utils/exceptions":12}],12:[function(require,module,exports){
'use strict';

function NegocioException(message) {
  this.name = 'NegocioException';
  this.message= message;
}
NegocioException.prototype = new Error();
NegocioException.prototype.constructor = NegocioException;

exports.negocio = NegocioException;

},{}],13:[function(require,module,exports){
'use strict';

/**
 * ConsultaVO
 * @param filtros
 *    Objeto com os campos para aplicar o filtro na consulta
 * @param dados
 *    Lista de objetos com o resultado da consulta
 * @param lazyLoad - Opcional
 *    Default = False
 * @param pagina - Opcional
 *    Pagina atual (paginacao)
 * @param totalPorPagina - Opcional
 *    Quantidade de registros por pagina
 */
function ConsultaVO(filtros, dados, lazyLoad, pagina, totalPorPagina) {
  this.filtros = Default(filtros, {});

  this.dados = Default(dados, {});
  this.total = 0;

  this.lazyLoad = Default(lazyLoad, false);

  this.pagina = Default(pagina, 1);
  this.totalPorPagina = Default(totalPorPagina, 5);

  this.configuracaoTotalPagina = [5, 10, 25];
}

ConsultaVO.prototype = new Object();
ConsultaVO.prototype.constructor = ConsultaVO;

exports.ConsultaVO = ConsultaVO;


/*
 * Funcoes utilitarias
 */
function Default(variable, new_value)
{
  if(new_value === undefined) {
    return (variable === undefined) ? null : variable;
  }

  return (variable === undefined) ? new_value : variable;
}

},{}]},{},[1]);
